import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import {GraphQLGatewayModule} from "@nestjs/graphql";

@Module({
  imports: [
      GraphQLGatewayModule.forRoot({
        server: {
          cors: true
        },
        gateway: {
          serviceList: [
            { name: 'user', url: 'http://localhost:3001/graphql'},
            { name: 'post', url: 'http://localhost:3002/graphql'}
          ]
        }
      })
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
